package com.project.ccs.endpoints;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


import com.project.ccs.services.CountryContinentService;
import com.project.ccs.oxm.Country;
import com.project.ccs.oxm.GetCountriesByContinentRequest;
import com.project.ccs.oxm.GetCountriesByContinentResponse;


/**
 * This is the message sendng and recievind contact point for the client applications.
 *	@Endpoint registers the class with Spring WS as a potential candidate for processing 
 *		incoming SOAP messages.
 *
 *	@PayloadRoot is then used by Spring WS to pick the handler method based on the 
 *		message�s namespace and localPart.
 *
 *	@RequestPayload indicates that the incoming message will be mapped to the method�s 
 *		request parameter.
 *
 *	@ResponsePayload annotation makes Spring WS map the returned value to the response payload.
 */


@Endpoint
public class ContinentCountryServiceEndpoint {
	private static final String TARGET_NAMESPACE = "http:///com/project/ccs/oxm";
	private static final Logger logger = Logger.getLogger(ContinentCountryServiceEndpoint.class);
	
	@Autowired
	private CountryContinentService service;
	
	@PayloadRoot(localPart = "getCountriesByContinentRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload GetCountriesByContinentResponse getCountries(
			@RequestPayload GetCountriesByContinentRequest request)
	{
		logger.debug("Invoking the getCountriesByContinentRequest().");
		
		GetCountriesByContinentResponse  response = new GetCountriesByContinentResponse();
		List<Country> countryList =  service.getCountriesByContinent(request.getContinent());
		response.getCountries().addAll(countryList);
		
		return response;
	}


	public void setService(CountryContinentService service) {
		this.service = service;
	}
	
	
	
	
	

}
