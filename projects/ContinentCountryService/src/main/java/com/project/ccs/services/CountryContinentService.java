package com.project.ccs.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;


import com.project.ccs.oxm.Country;
import com.project.ccs.oxm.Continent;

@Service
public class CountryContinentService {
	
	private static final Logger logger = Logger.getLogger(CountryContinentService.class);
	public List<Country> getCountriesByContinent(Continent continent){
		List<Country> countryList =  new ArrayList<Country>();
		
		if(continent.getContinentName().equalsIgnoreCase("Asia")){
			Country india= new Country();
			india.setContinent("asia");
			india.setCountryName("India");
			india.setDescription("India - description");
			// test commit
			
			
			Country pak =  new Country();
			pak.setContinent("asia");
			pak.setCountryName("pak");
			pak.setDescription("Pak- Descritipon");
			
			Country china = new Country();
			china.setContinent("asia");
			china.setCountryName("China");
			china.setDescription("China- Description");
			
			countryList.add(india);
			countryList.add(pak);
			countryList.add(china);
			
		}
		if(continent.getContinentName().equalsIgnoreCase("Europe")){
			Country india= new Country();
			india.setContinent("Europe");
			india.setCountryName("France");
			india.setDescription("France - description");
			
			
			Country pak =  new Country();
			pak.setContinent("Europe");
			pak.setCountryName("Ireland");
			pak.setDescription("Ireland- Descritipon");
			
			Country china = new Country();
			china.setContinent("Europe");
			china.setCountryName("England");
			china.setDescription("England- Description");
			
			countryList.add(india);
			countryList.add(pak);
			countryList.add(china);
		}
		if(continent.getContinentName().equalsIgnoreCase("America")){
			Country india= new Country();
			india.setContinent("America");
			india.setCountryName("Canada");
			india.setDescription("Canada - description");
			
			
			Country pak =  new Country();
			pak.setContinent("America");
			pak.setCountryName("Mexico");
			pak.setDescription("Mexico- Descritipon");
			
			Country china = new Country();
			china.setContinent("America");
			china.setCountryName("USA");
			china.setDescription("USA- Description");
			
			countryList.add(india);
			countryList.add(pak);
			countryList.add(china);
		}
		
		
		logger.debug("Invoking the service layer invoked."+ countryList.toString());
		return countryList;
		
	}

}
